package com.apress.ejb.chapter02;

import java.util.logging.Logger;

import javax.ejb.DependsOn;
import javax.ejb.Schedule;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.Timer;

@Singleton
@Startup
@DependsOn("ShopperCount")
public class LogShopperCount {
	private final Logger log = Logger.getLogger("LogShopperCount.class");
	Timer timer;
	
	// Logs shopper count every 2 hours
	@Schedule(hour="*/2")
	public void logShopperCount() {
		// log shopper count
		String timerInfo = (String)timer.getInfo();
		System.out.println(timerInfo);
	}
}
