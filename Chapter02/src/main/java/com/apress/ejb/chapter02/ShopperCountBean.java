package com.apress.ejb.chapter02;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.Singleton;
import javax.ejb.Startup;

@Singleton(name="ShopperCount")
@Startup
public class ShopperCountBean {
	private int shopperCounter;
	
	public void incrementShopperCount() {
		shopperCounter++;
	}
	
	public int getShopperCount() {
		return shopperCounter;
	}
	
	public void resetCounter() {
		shopperCounter = 0;
	}
	
	@PostConstruct
	public void applicationStartup() {
		System.out.println("From applicationStartup method");
		resetCounter();
	}
	
	@PreDestroy
	public void applicationShutdown() {
		System.out.println("From application shut down method");
	}

}
